var fs = require('fs');
var PeerServer = require('peer').PeerServer;

var server = PeerServer({
  port: 9000,
  ssl: {
    key: fs.readFileSync('/home/zek/projeler/vildan-bitirme/peerssl/peerjs-selfsigned.key'),
    cert: fs.readFileSync('/home/zek/projeler/vildan-bitirme/peerssl/peerjs-selfsigned.crt')
  },
  path: '/test'
});